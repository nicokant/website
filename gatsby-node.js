/**
 * Implement Gatsby's Node APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/node-apis/
 */

const path = require("path")

if (process.env.NODE_ENV === 'development') {
  process.env.GATSBY_WEBPACK_PUBLICPATH = '/'
}

exports.createPages = ({ actions, graphql }) => {
  const { createPage } = actions

  const workTemplate = path.resolve(`src/templates/works.js`)

  return graphql(`
    {
      allMarkdownRemark(
        sort: { order: DESC, fields: [frontmatter___title] }
        filter: { fields: { collection: { eq: "works" } } }
        limit: 1000
      ) {
        edges {
          node {
            fields {
              collection
            }
            frontmatter {
              path
            }
          }
        }
      }
    }
  `).then(result => {
    if (result.errors) {
      return Promise.reject(result.errors)
    }

    const posts = result.data.allMarkdownRemark.edges
    const postsPerPage = 10
    const numPages = Math.ceil(posts.length / postsPerPage)
    Array.from({ length: numPages }).forEach((_, i) => {
      createPage({
        path: i === 0 ? `/works` : `/works/${i + 1}`,
        component: path.resolve("./src/pages/works.js"),
        context: {
          limit: postsPerPage,
          skip: i * postsPerPage,
          numPages,
          currentPage: i + 1,
        },
      })
    })

    result.data.allMarkdownRemark.edges.forEach(({ node }) => {
      createPage({
        path: node.frontmatter.path,
        component: workTemplate,
        context: {}, // additional data can be passed via context
      });
    })
  })
}

exports.onCreateNode =({ node, getNode, actions }) => {
  if (node.internal.type === 'MarkdownRemark') {
      const { createNodeField } = actions;
      createNodeField({
        name: `collection`,
        node,
        value: getNode(node.parent).sourceInstanceName
      });
  }
}
