---
path: /works/math-melodies
title: Math Melodies 2
role: Full Stack Developer
featured: false
startdate: 2017-11
summary: Cross Platform mobile game to teach maths to visually impaired kids
active: true
technologies:
  - react native
  - accessibility
links:
  - label: Paper
    link: https://dl.acm.org/citation.cfm?id=3241006
cover: ./mathmelodies.png
---

Math Melodies 2 is a porting to cross platform of the Math Melodies app developed by Everyware Labs for iOS.
The app is focused on teaching Maths to children, with the specific target of being completly accessible
also to visually impaired children.

The app is also an exploration of how a cross platform framework like React Native is able to handle accessibility
and which are the limits if you want to develop an interactive accessible game using cross platform tools.

The results of this exploration have been published and are available online:
- [Math Melodies 2](https://dl.acm.org/doi/10.1145/3234695.3241006)
