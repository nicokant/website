---
path: /works/tlab
title: TLab IoT
role: Frontend Developer
featured: false
startdate: 2020-02
summary: Backoffice to manage installation of IoT devices
active: true
technologies:
  - react
  - firebase
cover: ./tlab.png
---

