---
path: /works/data-manager
title: Clinical Data Manager
role: Full Stack Developer
featured: false
startdate: 2018-09
summary: A data manager with visual query support
active: true
technologies:
  - react
  - django
links: []
cover: ./data-manager.png
---
Developed to migrate patients data from a huge excel table to a more structured database, these platform allows
doctors to store and easily retrive informations of recoveries and treatments. It also features a visual query builder
to allow complex queries to be graphically built and a column selector.

The backend is built upon Django while the frontend is powered by React
