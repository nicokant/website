---
path: /works/app-exam-recorder
title: Oral Exam Recorder
role: Mobile Developer
featured: false
startdate: 2018-11
summary: A mobile application for teachers to record oral exams for logging purpose
active: false
technologies:
  - react native
  - android
  - ios
  - flask
links: []
cover: ./exam-recorder.png
---
A Mobile application to allow teachers document and log the exams of candidates. Teachers are able to record and upload users oral exam sessions to a server, 
they can also scan and uplaod groups of photos related to written exams.
