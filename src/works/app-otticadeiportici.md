---
path: /works/app-lens-booking
title: Contact lenses booking
role: Full Stack Developer
featured: false
startdate: 2016-03
summary: A mobile application to book contact lenses and get notified when it's time to replace them
active: true
technologies:
  - react native
  - android
  - firebase
  - react
links:
cover: ./app-ottica.png
---

Mobile Application to allow users to book their contact lenses at Ottica dei Portici and get notified when are ready to be picked up. 
It also features a day counter that notifies the user when their lenses should be replaced.

Admin have a complete backoffice that allows them to manage users' orders.

I'm working on a complete rewrite of the application, moving from firebase realtime database to a custom backend developed with Django
and a new admin interface in React. I'm also planning to release the full source code of this project!
