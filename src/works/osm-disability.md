---
path: /works/osm-disability
title: OSM Accessibility Map
role: Frontend Developer
featured: true
startdate: 2020-12
summary: a Map which highlights Open Street Map POIs and their accessibility
active: true
technologies:
  - react
  -  leaflet
cover: ./osm.png
---

