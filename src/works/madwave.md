---
path: /works/madwave
title: Mad Wave
role: Full Stack Developer
featured: true
startdate: 2020-06
summary: Real Time Online Conference Platform
active: true
technologies:
  - react
  - django
  - websocket
  - channels
cover: ./madwave.png
---
