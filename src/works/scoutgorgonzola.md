---
path: /works/scout
title: Scout Gorgonzola Website
role: Theme Developer
featured: false
startdate: 2018-12
summary: Custom made WP theme for the scout group of Gorgonzola (MI)
active: false
technologies:
  - wordpress
  - genesis framework
links:
 - label: Website
   link: https://scoutgorgonzola.it
cover: ./scout.png
---
