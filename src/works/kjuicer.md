---
path: /works/kjuicer
title: Kjuicer
role: Full Stack Developer
featured: true
startdate: 2015-03
summary: Webapp to extract knowledge from texts you write and read using highlighters
active: true
technologies:
  - react
  - js
  - django
  - python
  - postgresql
links:
 - label: Website
   link: https://kjuicer.com
 - label: App
   link: https://beta.kjuicer.com
cover: ./kjuicer.png
---

Kjuicer is a startup based in Milan, founded by Francesco Frassinelli and Giampaolo Ferradini.
I joined Kjuicer in 2015 as Web developer, mostly working on the frontend of the application.
The webapp aims to simplify how we deal with huge sets of text, like books and long articles,
allowing you to highlight parts of it that are then turn on shorter summaries.

My main contribution is the development of a specific editor which allows users to write highlightable
text that support kjuicer summary natively.

