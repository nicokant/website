---
path: /works/pinguando
title: Pinguando Biz
role: Frontend Developer
featured: false
startdate: 2019-08
summary: Backoffice for car insurance's brokers
active: false
technologies:
  - react
links:
  - label: Pinguando app
    link: https://pinguando.it/
cover: ./pinguando.png
---

Backoffice for brokers that allows them to generate simulations and proposals for new 
car insurances.

The solution is heavily based on React JSON Schema and Redux to allow dynamic changes to the JSON schema structure
that is validated in Real Time. 
