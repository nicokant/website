---
path: /works/ottica-dei-portici
title: Ottica dei Portici Website
role: WP Theme Developer
featured: false
startdate: 2018-10
summary: Custom made WP theme for an Optical store
active: false
technologies:
  - Wordpress
  - Genesis Framework
links:
 - label: Website
   link: https://otticadeiporticitreviglio.it
cover: ./otticadeiportici.png
---
Wordpress Custom Theme for Ottica dei Portici
