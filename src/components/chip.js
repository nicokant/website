import React from 'react';

export default ({ children, text }) => (
  <span className="bg-warning px-2 py-1 rounded mx-1 my-2">{text ? text : children}</span>
);
